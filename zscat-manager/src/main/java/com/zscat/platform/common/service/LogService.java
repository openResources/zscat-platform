package com.zscat.platform.common.service;

import com.zscat.common.utils.Query;
import com.zscat.platform.common.domain.LogDO;
import com.zscat.platform.common.domain.PageDO;
import org.springframework.stereotype.Service;

@Service
public interface LogService {
	PageDO<LogDO> queryList(Query query);
	int remove(Long id);
	int batchRemove(Long[] ids);
}
