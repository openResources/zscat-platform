package com.zscat.platform.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zscat.common.utils.PageUtils;
import com.zscat.common.utils.Query;
import com.zscat.common.utils.R;
import com.zscat.user.entity.FavoriteDO;
import com.zscat.user.service.FavoriteService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zscat
 * @email 951449465@qq.com
 * @date 2017-10-21 11:21:45
 */

@Controller
@RequestMapping("/shop/favorite")
public class FavoriteController {
    @Reference(
            version = "${web.service.version}",
            application = "${dubbo.application.id}",
            registry = "${dubbo.registry.id}"
    )
    private FavoriteService favoriteService;

    @GetMapping()
    @RequiresPermissions("shop:favorite:favorite")
    String Favorite() {
        return "shop/favorite/favorite";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("shop:favorite:favorite")
    public PageUtils list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);
        List<FavoriteDO> favoriteList = favoriteService.list(query);
        int total = favoriteService.count(query);
        PageUtils pageUtils = new PageUtils(favoriteList, total);
        return pageUtils;
    }

    @GetMapping("/add")
    @RequiresPermissions("shop:favorite:add")
    String add() {
        return "shop/favorite/add";
    }

    @GetMapping("/edit/{id}")
    @RequiresPermissions("shop:favorite:edit")
    String edit(@PathVariable("id") Long id, Model model) {
        FavoriteDO favorite = favoriteService.get(id);
        model.addAttribute("favorite", favorite);
        return "shop/favorite/edit";
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    @RequiresPermissions("shop:favorite:add")
    public R save(FavoriteDO favorite) {
        if (favoriteService.save(favorite) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    @RequiresPermissions("shop:favorite:edit")
    public R update(FavoriteDO favorite) {
        favoriteService.update(favorite);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    @RequiresPermissions("shop:favorite:remove")
    public R remove(Long id) {
        if (favoriteService.remove(id) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    @RequiresPermissions("shop:favorite:batchRemove")
    public R remove(@RequestParam("ids[]") Long[] ids) {
        favoriteService.batchRemove(ids);
        return R.ok();
    }

}
