package com.zscat.platform.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zscat.common.utils.PageUtils;
import com.zscat.common.utils.Query;
import com.zscat.common.utils.R;
import com.zscat.order.entity.TGoodSorderDO;
import com.zscat.order.service.TGoodSorderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zscat
 * @email 951449465@qq.com
 * @date 2017-10-15 15:07:36
 */

@Controller
@RequestMapping("/shop/tGoodSorder")
public class TGoodSorderController {
    @Reference(
            version = "${web.service.version}",
            application = "${dubbo.application.id}",
            registry = "${dubbo.registry.id}"
    )
    private TGoodSorderService tGoodSorderService;

    @GetMapping()
    @RequiresPermissions("shop:tGoodSorder:tGoodSorder")
    String TGoodSorder() {
        return "shop/tGoodSorder/tGoodSorder";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("shop:tGoodSorder:tGoodSorder")
    public PageUtils list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);
        List<TGoodSorderDO> tGoodSorderList = tGoodSorderService.list(query);
        int total = tGoodSorderService.count(query);
        PageUtils pageUtils = new PageUtils(tGoodSorderList, total);
        return pageUtils;
    }

    @GetMapping("/add")
    @RequiresPermissions("shop:tGoodSorder:add")
    String add() {
        return "shop/tGoodSorder/add";
    }

    @GetMapping("/edit/{id}")
    @RequiresPermissions("shop:tGoodSorder:edit")
    String edit(@PathVariable("id") Long id, Model model) {
        TGoodSorderDO tGoodSorder = tGoodSorderService.get(id);
        model.addAttribute("tGoodSorder", tGoodSorder);
        return "shop/tGoodSorder/edit";
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    @RequiresPermissions("shop:tGoodSorder:add")
    public R save(TGoodSorderDO tGoodSorder) {
        if (tGoodSorderService.save(tGoodSorder) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    @RequiresPermissions("shop:tGoodSorder:edit")
    public R update(TGoodSorderDO tGoodSorder) {
        tGoodSorderService.update(tGoodSorder);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    @RequiresPermissions("shop:tGoodSorder:remove")
    public R remove(Long id) {
        if (tGoodSorderService.remove(id) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    @RequiresPermissions("shop:tGoodSorder:batchRemove")
    public R remove(@RequestParam("ids[]") Long[] ids) {
        tGoodSorderService.batchRemove(ids);
        return R.ok();
    }

}
