package com.zscat.platform.common.controller;

import com.zscat.platform.common.constant.AdminConstant;
import com.zscat.platform.system.domain.UserDO;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;

@Controller
public class BaseController {
    public UserDO getUser(HttpServletRequest request) {
        return (UserDO) request.getSession().getAttribute(AdminConstant.USER_SESSION_KEY);
    }

    public Long getUserId(HttpServletRequest request) {
        return getUser(request).getUserId();
    }

    public String getUsername(HttpServletRequest request) {
        return getUser(request).getUsername();
    }

    public String getUsername() {
        return "111111";
    }

    public UserDO getLoginUser(HttpServletRequest request) {
        return (UserDO) request.getSession().getAttribute(AdminConstant.USER_SESSION_KEY);
    }
}