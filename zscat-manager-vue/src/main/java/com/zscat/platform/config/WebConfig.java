package com.zscat.platform.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class WebConfig implements WebMvcConfigurer {


    @Value("${cookie.domian.name}")
    private String cookieDomianName;


    @Bean
    public CookieSerializer defaultCookieSerializer() {
        DefaultCookieSerializer defaultCookieSerializer = new DefaultCookieSerializer();
        defaultCookieSerializer.setCookieName("sessionId");
        defaultCookieSerializer.setDomainName(cookieDomianName);
        defaultCookieSerializer.setCookiePath("/");
        return defaultCookieSerializer;
    }

}
