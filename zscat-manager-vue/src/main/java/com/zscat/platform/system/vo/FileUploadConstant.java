package com.zscat.platform.system.vo;


import com.zscat.platform.system.util.FilePathUtil;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * 上传文件常量类定义
 *
 * @author yang.liu
 */
public class FileUploadConstant {
    /**
     * 设置上传文件最大为5M
     **/
    public static final int IMAGE_UPLOAD_MAX_SIZE = 50 * 1024 * 1024;

    /**
     * 允许上传的文件格式的列表
     */
    @SuppressWarnings("serial")
    public static final Set<String> IMAGE_UPLOAD_ALLOW_TYPE = new HashSet<String>() {

        {
            add(".bmp");
            add(".gif");
            add(".jpg");
            add(".jpeg");
            add(".png");
            add(".mp4");
        }
    };

    /**
     * 上传返回代码-成功
     */
    public static final int SUCCESS = 200;
    /**
     * 上传返回代码-参数错误
     */
    public static final int PARAM_ERROR = 400;
    /**
     * 上传返回代码-没有上传文件
     */
    public static final int NO_FILE_ERROR = 401;
    /**
     * 上传返回代码-空文件
     */
    public static final int EMPTY_FILE_ERROR = 402;
    /**
     * 上传返回代码-不支持的文件类型
     */
    public static final int NOT_SUPPORT_TYPE_ERROR = 403;
    /**
     * 上传返回代码-文件太大
     */
    public static final int FILE_TOO_BIG_ERROR = 404;
    /**
     * 上传返回代码-没有登录
     */
    public static final int NOT_LOGIN_ERROR = 461;
    /**
     * 上传返回代码-上传失败
     */
    public static final int SYSTEM_ERROR = 500;

    public static File TEMP_DIR;

    static {
        String webRoot = FilePathUtil.getAbsolutePathOfWebRoot();
        TEMP_DIR = new File(webRoot + "/tmp/");
    }

}
