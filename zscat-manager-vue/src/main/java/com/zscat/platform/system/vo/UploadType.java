package com.zscat.platform.system.vo;

/**
 * 上传类型枚举类型定义
 *
 * @author yang.liu
 */
public enum UploadType {
    /**
     * 图片
     **/
    image,
    /**
     * 文件
     **/
    file
}
