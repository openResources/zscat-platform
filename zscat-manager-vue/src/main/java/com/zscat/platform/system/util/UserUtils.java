package com.zscat.platform.system.util;

import com.zscat.platform.common.constant.AdminConstant;
import com.zscat.platform.system.domain.UserDO;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: shenzhuan
 * @Date: 2018/12/21 22:10
 * @Description:
 */
public class UserUtils {
    public static UserDO getLoginUser(HttpServletRequest request) {
        return (UserDO) request.getSession().getAttribute(AdminConstant.USER_SESSION_KEY);
    }
}
