package com.zscat.platform.system.controller;

import com.zscat.common.utils.PassordUtil;
import com.zscat.common.utils.Query;
import com.zscat.common.utils.R;
import com.zscat.common.utils.Tree;
import com.zscat.platform.common.annotation.Log;
import com.zscat.platform.common.controller.BaseController;
import com.zscat.platform.system.domain.DeptDO;
import com.zscat.platform.system.domain.RoleDO;
import com.zscat.platform.system.domain.UserDO;
import com.zscat.platform.system.service.RoleService;
import com.zscat.platform.system.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/sys/user")
@Controller
public class UserController extends BaseController {
    private String prefix = "system/user";
    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;

    @GetMapping("/list")
    @ResponseBody
    R list(@RequestParam Map<String, Object> params) {
        R r = new R();
        // 查询列表数据
        Query query = new Query(params);
        List<UserDO> sysUserList = userService.list(query);
        int total = userService.count(query);
        r.put("rows", sysUserList);
        r.put("total", total);
        return r;
    }

    // // @RequiresPermissions("sys:user:add")
    @Log("添加用户")
    @GetMapping("/add")
    String add(Model model) {
        Map<String, Object> params = new HashMap<>();
        List<RoleDO> roles = roleService.list(params);
        model.addAttribute("roles", roles);
        return prefix + "/add";
    }

    // // @RequiresPermissions("sys:user:edit")
    @Log("编辑用户")
    @GetMapping("/edit/{id}")
    String edit(Model model, @PathVariable("id") Long id) {
        UserDO userDO = userService.get(id);
        model.addAttribute("user", userDO);
        List<RoleDO> roles = roleService.list(id);
        model.addAttribute("roles", roles);
        return prefix + "/edit";
    }

    // // @RequiresPermissions("sys:user:user")
    @Log("编辑用户")
    @GetMapping("/detail")
    @ResponseBody
    R detail(Model model, Long userId) {
        R r = new R();
        UserDO userDO = userService.get(userId);
        r.put("user", userDO);
        List<RoleDO> roles = roleService.list(userId);
        r.put("roles", roles);
        return r;
    }

    // // @RequiresPermissions("sys:user:add")
    @Log("保存用户")
    @PostMapping("/save")
    @ResponseBody
    R save(UserDO user, HttpServletRequest request) {
        if ("test".equals(getUsername(request))) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        user.setPassword(PassordUtil.encryptPassord(user.getPassword()));
        if (user.getUserId() == null) {
            if (userService.save(user) > 0) {
                return R.ok();
            }
        } else {
            if (userService.update(user) > 0) {
                return R.ok();
            }
        }

        return R.error();
    }

    // // @RequiresPermissions("sys:user:edit")
    @Log("更新用户")
    @PostMapping("/update")
    @ResponseBody
    R update(UserDO user, HttpServletRequest request) {
        if ("test".equals(getUsername(request))) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        if (userService.update(user) > 0) {
            return R.ok();
        }
        return R.error();
    }

    // // @RequiresPermissions("sys:user:remove")
    @Log("删除用户")
    @PostMapping("/remove")
    @ResponseBody
    R remove(Long id, HttpServletRequest request) {
        if ("test".equals(getUsername(request))) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        if (userService.remove(id) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 批量删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    public R remove(String userIds) {
        if (StringUtils.isBlank(userIds)) {
            return R.error();
        }
        String[] ids = userIds.split(",");
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        userService.batchremove(ids);
        return R.ok();
    }

    @PostMapping("/exit")
    @ResponseBody
    boolean exit(@RequestParam Map<String, Object> params) {
        return !userService.exit(params);// 存在，不通过，false
    }

    // // @RequiresPermissions("sys:user:resetPwd")
    @Log("请求更改用户密码")
    @GetMapping("/resetPwd/{id}")
    String resetPwd(@PathVariable("id") Long userId, Model model) {

        UserDO userDO = new UserDO();
        userDO.setUserId(userId);
        model.addAttribute("user", userDO);
        return prefix + "/reset_pwd";
    }


    @GetMapping("/tree")
    @ResponseBody
    public Tree<DeptDO> tree() {
        Tree<DeptDO> tree = new Tree<DeptDO>();
        tree = userService.getTree();
        return tree;
    }

    @GetMapping("/treeView")
    String treeView() {
        return prefix + "/userTree";
    }

}
