package com.zscat.platform.system.vo;

import java.util.HashMap;
import java.util.Map;

/**
 * 阿里云OSS Bucket 枚举类型定义
 *
 * @author zscat
 */
public enum BucketEnums {

    /**
     * 系统级别资源，存储系统内公用资源
     **/
    FOP_SYSTEM_RESOURCE("fop-system-resource"),

    /**
     * 用户级别资源，存储用户头像、个人微信二维码等相关资源
     **/
    FOP_USER_RESOURCE("fop-user-resource"),

    /**
     * 商品级别资源，存储商品、订单等相关资源
     **/
    FOP_GOODS_RESOURCE("fop-goods-resource"),

    /**
     * 健康技术级别资源，存储健康技术相关图片，视频等资源
     **/
    FOP_HEALTH_RESOURCE("fop-health-resource"),

    /**
     * 市场推广级别资源，存储市场活动向外投放的广告图片，视频等资源
     **/
    FOP_MARKETING_RESOURCE("fop-marketing-resource"),

    /**
     * 减脂营级别资源，存储学员打卡图片等资源
     **/
    FOP_FITCAMP_RESOURCE("fop-fitcamp-resource"),

    /**
     * 辣妈营级别资源，存储学员打卡图片等资源
     **/
    FOP_HOTCAMP_RESOURCE("fop-hotcamp-resource"),
    /**
     * 辣妈营级别资源，存储学员打卡图片等资源
     **/
    FOP_SHOPMS_RESOURCE("fop-shopms-resource");;


    private String bucketName;
    private static final Map<String, BucketEnums> BUCKET_MAPS = new HashMap<String, BucketEnums>();

    static {
        for (BucketEnums bucket : BucketEnums.values()) {
            BUCKET_MAPS.put(bucket.getBucketName(), bucket);
        }
    }

    public static BucketEnums getEnum(String bucketName) {
        return BUCKET_MAPS.get(bucketName);
    }


    public static String getSelectStr() {
        StringBuffer sb = new StringBuffer("<select id='bucket' class='txt' name='bucket'>");
        for (BucketEnums bucket : BucketEnums.values()) {
            sb.append("<option value='" + bucket.getBucketName() + "'>" + bucket.getBucketName() + "</option>");
        }
        sb.append("</select>");
        return sb.toString();
    }


    private BucketEnums(String bucketName) {
        this.bucketName = bucketName;
    }


    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

}
