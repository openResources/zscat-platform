package com.zscat.platform.system.service;


import com.zscat.platform.system.vo.BucketEnums;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * 阿里云OSS服务接口定义
 *
 * @author zscat
 */
public interface AliOssService {
    /**
     * 上传文件到阿里云OSS
     *
     * @param data
     * @param fileSuffix
     * @return
     * @throws IOException
     */
    String upload(byte[] data, String fileSuffix) throws IOException;

    /**
     * 上传文件到阿里云OSS
     *
     * @param bucket
     * @param data
     * @param fileSuffix
     * @return
     * @throws IOException
     */
    String upload(BucketEnums bucket, byte[] data, String fileSuffix) throws IOException;

    /**
     * 上传文件到阿里云OSS
     *
     * @param bucket
     * @param is
     * @param fileSuffix
     * @param aliyunOssResizeStr
     * @return
     * @throws IOException
     */
    String upload(BucketEnums bucket, InputStream is, String fileSuffix, String aliyunOssResizeStr) throws IOException;

    /**
     * 上传文件到阿里云OSS
     *
     * @param bucket
     * @param objectName
     * @param data
     * @param metaMapString
     * @return
     * @throws IOException
     */
    String upload(BucketEnums bucket, String objectName, byte[] data, Map<String, String> metaMapString) throws IOException;

    /**
     * 上传文件到阿里云OSS
     *
     * @param bucket
     * @param objectName
     * @param is
     * @param metaMap
     * @param aliyunOssResizeStr
     * @return
     * @throws IOException
     */
    String upload(BucketEnums bucket, String objectName, InputStream is, Map<String, String> metaMap, String aliyunOssResizeStr) throws IOException;
}
