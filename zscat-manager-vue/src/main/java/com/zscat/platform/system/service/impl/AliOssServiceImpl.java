package com.zscat.platform.system.service.impl;


import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import com.zscat.platform.system.service.AliOssService;
import com.zscat.platform.system.vo.BucketEnums;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 阿里云OSS服务接口实现类定义
 *
 * @author zscat
 */
@Service
public class AliOssServiceImpl implements AliOssService {
    /**
     * 阿里云OSS内网访问地址
     **/
    @Value("${aliyun.oss.internal.endpoint}")
    private static String aliyunOssInternalEndpoint = null;
    /**
     * 阿里云OSS外网访问地址
     **/
    @Value("${aliyun.oss.external.endpoint}")
    private static String aliyunOssExternalEndpoint = null;
    /**
     * 阿里云OSS CDN访问地址
     **/
    @Value("${aliyun.oss.cdn.endpoint}")
    private static String aliyunOssCdnEndpoint = null;
    /**
     * 是否启用阿里云OSS CDN域名访问
     **/
    @Value("${aliyun.if.use.oss.cdn.endpoint}")
    private static boolean aliyunIfUseOssCdnEndpoint = true;
    /**
     * 阿里云OSS内网访问地址
     **/
    @Value("${aliyun.oss.key}")
    private static String aliyunOssKey = null;
    /**
     * 阿里云OSS内网访问地址
     **/
    @Value("${aliyun.oss.secret}")
    private static String aliyunOssSecret = null;

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 默认上传的是减脂营资源
     *
     * @param data 字节流
     * @return id
     * @throws IOException
     */
    @Override
    public String upload(byte[] data, String fileSuffix) throws IOException {
        return upload(BucketEnums.FOP_FITCAMP_RESOURCE, data, fileSuffix);
    }

    @Override
    public String upload(BucketEnums bucket, byte[] data, String fileSuffix) throws IOException {
        return upload(bucket, UUID.randomUUID().toString() + fileSuffix, data, new LinkedHashMap<String, String>());
    }

    @Override
    public String upload(BucketEnums bucket, InputStream is, String fileSuffix, String aliyunOssResizeStr) throws IOException {
        return upload(bucket, UUID.randomUUID().toString() + fileSuffix, is, new LinkedHashMap<String, String>(), aliyunOssResizeStr);
    }

    @Override
    public String upload(BucketEnums bucket, String objectName, byte[] data, Map<String, String> metaMap) throws IOException {
        OSSClient ossClient = new OSSClient(aliyunOssInternalEndpoint, aliyunOssKey, aliyunOssSecret);
        ObjectMetadata meta = new ObjectMetadata();
        InputStream is = new ByteArrayInputStream(data);

        if (objectName.endsWith(".jpg")) {
            meta.setContentType("image/jpg");
        } else if (objectName.endsWith(".jpeg")) {
            meta.setContentType("image/jpeg");
        } else if (objectName.endsWith(".png")) {
            meta.setContentType("image/png");
        } else if (objectName.endsWith(".gif")) {
            meta.setContentType("image/gif");
        }
        meta.setContentLength(is.available());
        if (metaMap != null) {
            for (Map.Entry<String, String> entry : metaMap.entrySet()) {
                meta.addUserMetadata(entry.getKey(), entry.getValue());
            }
        }
        PutObjectResult result = null;

        try {
            result = ossClient.putObject(bucket.getBucketName(), objectName, is, meta);
        } catch (Exception ex) {//try again once.
            result = ossClient.putObject(bucket.getBucketName(), objectName, is, meta);
            ex.printStackTrace();
        }

        logger.info(result.getETag() + ", " + objectName + ", length:" + meta.getContentLength() + ",result.getRequestId():" + result.getRequestId());

        if (aliyunIfUseOssCdnEndpoint) {
            return processCDNFileUrl(aliyunOssCdnEndpoint, bucket, objectName, "");
        } else {
            return processFileUrl(aliyunOssExternalEndpoint, bucket, objectName, "");
        }
    }

    @Override
    public String upload(BucketEnums bucket, String objectName, InputStream is, Map<String, String> metaMap, String aliyunOssResizeStr) throws IOException {
        OSSClient ossClient = new OSSClient(aliyunOssInternalEndpoint, aliyunOssKey, aliyunOssSecret);
        ObjectMetadata meta = new ObjectMetadata();

        if (objectName.endsWith(".jpg")) {
            meta.setContentType("image/jpg");
        } else if (objectName.endsWith(".jpeg")) {
            meta.setContentType("image/jpeg");
        } else if (objectName.endsWith(".png")) {
            meta.setContentType("image/png");
        } else if (objectName.endsWith(".gif")) {
            meta.setContentType("image/gif");
        }
        meta.setContentLength(is.available());
        if (metaMap != null) {
            for (Map.Entry<String, String> entry : metaMap.entrySet()) {
                meta.addUserMetadata(entry.getKey(), entry.getValue());
            }
        }
        PutObjectResult result = ossClient.putObject(bucket.getBucketName(), objectName, is, meta);
        logger.info(result.getETag() + ", " + objectName + ", length:" + meta.getContentLength() + ",result.getRequestId():" + result.getRequestId());

        if (aliyunIfUseOssCdnEndpoint) {
            return processCDNFileUrl(aliyunOssCdnEndpoint, bucket, objectName, aliyunOssResizeStr);
        } else {
            return processFileUrl(aliyunOssExternalEndpoint, bucket, objectName, aliyunOssResizeStr);
        }
    }

    /**
     * 处理文件URL地址
     *
     * @param endpoint
     * @param bucket
     * @param objectName
     * @return
     */
    public static String processFileUrl(String endpoint, BucketEnums bucket, String objectName, String aliyunOssResizeStr) {
        return endpoint.replace("https://", "https://" + bucket.getBucketName() + ".") + "/" + objectName + aliyunOssResizeStr;
    }

    /**
     * 处理CDN文件URL地址
     *
     * @param endpoint
     * @param bucket
     * @param objectName
     * @return
     */
    public static String processCDNFileUrl(String endpoint, BucketEnums bucket, String objectName, String aliyunOssResizeStr) {
        return endpoint.replace("BUCKET", bucket.getBucketName()) + "/" + objectName + aliyunOssResizeStr;
    }


    public static String getAliyunOssInternalEndpoint() {
        return aliyunOssInternalEndpoint;
    }

    public static void setAliyunOssInternalEndpoint(String aliyunOssInternalEndpoint) {
        AliOssServiceImpl.aliyunOssInternalEndpoint = aliyunOssInternalEndpoint;
    }

    public static String getAliyunOssExternalEndpoint() {
        return aliyunOssExternalEndpoint;
    }

    public static void setAliyunOssExternalEndpoint(String aliyunOssExternalEndpoint) {
        AliOssServiceImpl.aliyunOssExternalEndpoint = aliyunOssExternalEndpoint;
    }

    public static String getAliyunOssCdnEndpoint() {
        return aliyunOssCdnEndpoint;
    }

    public static void setAliyunOssCdnEndpoint(String aliyunOssCdnEndpoint) {
        AliOssServiceImpl.aliyunOssCdnEndpoint = aliyunOssCdnEndpoint;
    }

    public static boolean isAliyunIfUseOssCdnEndpoint() {
        return aliyunIfUseOssCdnEndpoint;
    }

    public static void setAliyunIfUseOssCdnEndpoint(boolean aliyunIfUseOssCdnEndpoint) {
        AliOssServiceImpl.aliyunIfUseOssCdnEndpoint = aliyunIfUseOssCdnEndpoint;
    }

    public static String getAliyunOssKey() {
        return aliyunOssKey;
    }

    public static void setAliyunOssKey(String aliyunOssKey) {
        AliOssServiceImpl.aliyunOssKey = aliyunOssKey;
    }

    public static String getAliyunOssSecret() {
        return aliyunOssSecret;
    }

    public static void setAliyunOssSecret(String aliyunOssSecret) {
        AliOssServiceImpl.aliyunOssSecret = aliyunOssSecret;
    }

}
