package com.zscat.platform.system.controller;


import com.zscat.platform.system.service.AliOssService;
import com.zscat.platform.system.vo.BucketEnums;
import com.zscat.platform.system.vo.FileUploadConstant;
import com.zscat.platform.system.vo.UploadType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 图片上传接口
 *
 * @author
 * @modify 修改为阿里云上传
 */
@Controller
public class FileUploadController {

    private static final String ACTION_CONFIG = "config";

    private static final String ACTION_UPLOADIMAGE = "uploadImage";
    private static final String ACTION_UEDITERUPLOADIMAGE = "uEditerUploadImage";
    private static final String ACTION_UPLOADFILE = "uploadFile";

    protected Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    AliOssService aliOssService;

    @RequestMapping("/uploadImage")
    public void upload(String action, String callback, HttpServletRequest request, HttpServletResponse response) {
        uploadToAliyunOss(action, callback, UploadType.image, request, response);
    }

    @RequestMapping("/uploadFile")
    public void uploadFile(String action, String callback, String plantform, String source, HttpServletRequest request, HttpServletResponse response) {
        uploadToAliyunOss(action, callback, UploadType.file, request, response);
    }

    private void uploadToAliyunOss(String action, String callback, UploadType type, HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> ret = new HashMap<>();
        if (ACTION_CONFIG.equals(action)) {
            writeResponseMessage(response, getUploadConfig(), callback);
            return;
        } else {
            if (!ACTION_UPLOADIMAGE.equals(action) && !ACTION_UEDITERUPLOADIMAGE.equals(action) && !ACTION_UPLOADFILE.equals(action)) {
                ret.put("retcode", -1);
                ret.put("retdesc", "invalid action");
                writeResponseMessage(response, ret, callback);
                return;
            }
        }

        List<String> fileNameList = new ArrayList<String>();

        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile file = multipartRequest.getFile("file");
        try {

            String fileName = file.getOriginalFilename();
            long size = file.getSize();
            // 验证是否是空文件
            if (StringUtils.isBlank(fileName) || size == 0) {
                ret.put("retcode", FileUploadConstant.EMPTY_FILE_ERROR);
                writeResponseMessage(response, ret, callback);
                return;
            }
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            //验证是否是允许上传的文件类型
            if (UploadType.image == type) {
                if (!FileUploadConstant.IMAGE_UPLOAD_ALLOW_TYPE.contains(suffix.toLowerCase())) {
                    ret.put("retcode", FileUploadConstant.NOT_SUPPORT_TYPE_ERROR);
                    writeResponseMessage(response, ret, callback);
                    return;
                }
            }

            fileNameList.add(uploadFileAndGenUrl(file, suffix, type, ret));

            if (!fileNameList.isEmpty()) {
                ret.put("retcode", FileUploadConstant.SUCCESS);
                ret.put("imageUrl", fileNameList.get(0));
            } else {
                log.error("Fail to upload image to qiniu: fileNameList is empty");
                ret.put("retcode", FileUploadConstant.SYSTEM_ERROR);
            }
            if (ACTION_UEDITERUPLOADIMAGE.equals(action)) {
                writeResponseMessage(response, getUeditUploadRet(ret), callback);
            } else {
                writeResponseMessage(response, ret, callback);
            }

            return;

        } catch (Exception e) {
            log.error("Fail to upload image to qiniu: inner error", e);
            ret.put("retcode", FileUploadConstant.SYSTEM_ERROR);
            writeResponseMessage(response, ret, callback);
            return;
        }
    }


    private String uploadFileAndGenUrl(MultipartFile file, String suffix, UploadType type, Map<String, Object> ret) throws Exception {
        // 创建本地临时文件
        File tmpFile = new File(FileUploadConstant.TEMP_DIR + "img" + System.nanoTime() + suffix);
        file.transferTo(tmpFile);

        InputStream is = new FileInputStream(tmpFile.getAbsolutePath());

        boolean imgFlag = false;
        if (UploadType.image == type) {
            imgFlag = true;
        }
        String url = aliOssService.upload(BucketEnums.FOP_HOTCAMP_RESOURCE, is, imgFlag ? ".png" : suffix, "");

        log.info("[aliyunUpload] url1:" + url);

        if (null == url) {
            url = aliOssService.upload(BucketEnums.FOP_HOTCAMP_RESOURCE, is, imgFlag ? ".png" : suffix, "");
            log.info("[aliyunUpload] url2:" + url);
        }

        if (UploadType.file == type) {
            String md5String = getMd5ByFile(tmpFile);
            ret.put("md5String", md5String);
        }
        FileUtils.forceDelete(tmpFile);
        return url;
    }

    private Map<String, Object> getUploadConfig() {
        Map<String, Object> map = new HashMap<>();
        map.put("imageActionName", "uEditerUploadImage");
        map.put("imageFieldName", "file");
        map.put("imageMaxSize", FileUploadConstant.IMAGE_UPLOAD_MAX_SIZE);
        map.put("imageAllowFiles", FileUploadConstant.IMAGE_UPLOAD_ALLOW_TYPE);
        map.put("imageUrlPrefix", "");
        return map;
    }

    private Map<String, Object> getUeditUploadRet(Map<String, Object> ret) {
        Map<String, Object> map = new HashMap<>();
        int code = (int) ret.get("retcode");
        map.put("state", code == 200 ? "SUCCESS" : "error");
        map.put("url", ret.get("imageUrl"));
        return map;
    }

    private void writeResponseMessage(HttpServletResponse response, Map<String, Object> ret, String callback) {

        response.addHeader("content-type", "text/html");
        response.setCharacterEncoding("UTF-8");
        String json = ret.toString();

        try {
            if (StringUtils.isNotBlank(callback)) {
                response.getWriter().write("<script>window.top." + callback + "('" + json + "');</script>");
            } else {
                response.getWriter().write(json);
            }
            response.getWriter().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 内存映射方式加密文件，防止大文件加密吃内存及效率问题
     *
     * @param file
     * @return
     */
    public String getMd5ByFile(File file) {
        String value = null;
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        try {
            MappedByteBuffer byteBuffer = in.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(byteBuffer);
            BigInteger bi = new BigInteger(1, md5.digest());
            value = bi.toString(16);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return value;
    }

    public static void main(String[] args) {

    }
}