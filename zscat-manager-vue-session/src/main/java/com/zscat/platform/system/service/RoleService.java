package com.zscat.platform.system.service;

import com.zscat.common.utils.Query;
import com.zscat.platform.system.domain.RoleDO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface RoleService {

    RoleDO get(Long id);

    List<RoleDO> list(Map<String, Object> params);

    int save(RoleDO role);

    int update(RoleDO role);

    int remove(Long id);

    List<RoleDO> list(Long userId);

    int batchremove(String[] ids);

    int count(Query query);
}
