package com.zscat.platform.system.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 后台后页面控制器类定义
 *
 * @author zscat
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String home() {
        return "/vue/index.html";
    }

    @RequestMapping("/index.html")
    public String index() {
        return "/vue/index.html";
    }


}
