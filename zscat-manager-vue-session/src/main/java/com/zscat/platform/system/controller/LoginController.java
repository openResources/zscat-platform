package com.zscat.platform.system.controller;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.zscat.common.constants.CommonConstant;
import com.zscat.common.result.ResultInfo;
import com.zscat.common.result.ResultInfoObject;
import com.zscat.common.utils.PassordUtil;
import com.zscat.common.utils.R;
import com.zscat.common.utils.Tree;
import com.zscat.platform.common.annotation.Log;
import com.zscat.platform.common.constant.AdminConstant;
import com.zscat.platform.common.controller.BaseController;
import com.zscat.platform.system.domain.IndexData;
import com.zscat.platform.system.domain.MenuDO;
import com.zscat.platform.system.domain.RoleDO;
import com.zscat.platform.system.domain.UserDO;
import com.zscat.platform.system.service.MenuService;
import com.zscat.platform.system.service.RoleService;
import com.zscat.platform.system.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class LoginController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MenuService menuService;
    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;

    @Log("请求访问主页")
    @GetMapping({"/index"})
    String index(Model model, HttpServletRequest request) {
        UserDO userDO = getUser(request);
        if (userDO == null) {
            return "/vue/index.html";
        }
        List<Tree<MenuDO>> menus = menuService.listMenuTree(userDO.getUserId());
        model.addAttribute("menuList", menus);
        model.addAttribute("name", userDO.getName());
        return "/vue/index.html";
    }

    @Log("登录")
    @GetMapping("/indexData")
    @ResponseBody
    R indexData(HttpServletRequest request) {
        R r = new R();
        UserDO userDO = getUser(request);
        List<Tree<MenuDO>> menus = menuService.listMenuTree(userDO.getUserId());
        r.put("menuList", menus);
        r.put("user", userDO);
        return r;
    }

    @RequestMapping("/login.html")
    public String toLogin(HttpServletRequest request, ModelMap modelMap) {
        UserDO user = getLoginUser(request);
        if (user != null) {
            return "/vue/index.html";
        }
        return "/vue/index.html";
    }

    @RequestMapping("/login")
    public String loggin(HttpServletRequest request, ModelMap modelMap) {
        UserDO user = getLoginUser(request);
        if (user != null) {
            return "/vue/index.html";
        }
        return "/vue/index.html";
    }

    @RequestMapping("/login.json")
    @ResponseBody
    public ResultInfoObject<IndexData> login(String account, String password, ModelMap modelMap, HttpServletRequest request) {
        ResultInfoObject<IndexData> resultInfo = new ResultInfoObject<IndexData>();
        if (StringUtils.isBlank(account) || StringUtils.isBlank(password)) {
            resultInfo.setCode(CommonConstant.PARAM_ERROR_CODE);
            resultInfo.setMsg("用户名密码不能为空");
            return resultInfo;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("username", account);


        // 查询用户信息
        UserDO user = userService.list(map).get(0);
        if (user == null) {
            modelMap.put("msg", "用户名或者密码不正确");
            resultInfo.setCode(CommonConstant.PARAM_ERROR_CODE);
            resultInfo.setMsg("用户名密码不能为空");
            return resultInfo;
        }
        if (user.getStatus() == 0) {
            resultInfo.setCode(CommonConstant.PARAM_ERROR_CODE);
            resultInfo.setMsg("用户无效");
            return resultInfo;
        }
        if (!user.getPassword().equals(PassordUtil.encryptPassord(password))) {
            resultInfo.setCode(CommonConstant.PARAM_ERROR_CODE);
            resultInfo.setMsg("用户名或者密码不正确");
            return resultInfo;
        }
        List<RoleDO> roles = roleService.list(user.getUserId());
        String showRoleName = "";
        StringBuilder showRoleNameBuilder = new StringBuilder();
        for (RoleDO role : roles) {
            showRoleNameBuilder.append(role.getRoleName() + " & ");
        }
        showRoleName = showRoleNameBuilder.toString();
        user.setShowRoleNames(showRoleName.substring(0, showRoleName.length() - 3));
        request.getSession().setAttribute(AdminConstant.USER_SESSION_KEY, user);
        resultInfo.setCode(CommonConstant.OPERATE_SUCCESS);
        resultInfo.setMsg("登陆成功");

        return resultInfo;
    }


    @RequestMapping("/checkLogin.json")
    @ResponseBody
    public ResultInfo checkLogin(HttpServletRequest request) {
        ResultInfo resultInfo = new ResultInfo();
        UserDO user = getUser(request);
        resultInfo.setRetcode(200);
        resultInfo.setMsg("登陆成功");
        if (user == null) {
            resultInfo.setRetcode(CommonConstant.RET_NOT_LOGIN);
            resultInfo.setCode(CommonConstant.RET_NOT_LOGIN);
            resultInfo.setMsg("未登陆");
        }
        return resultInfo;
    }

    @RequestMapping("/logout.json")
    @ResponseBody
    public ResultInfo logout(HttpServletRequest request) {
        ResultInfo resultInfo = new ResultInfo();
        request.getSession().removeAttribute(AdminConstant.USER_SESSION_KEY);
        resultInfo.setCode(CommonConstant.OPERATE_SUCCESS);
        resultInfo.setMsg("操作成功");
        return resultInfo;
    }

    @GetMapping("/main")
    String main() {
        return "/vue/index.html";
    }

    @GetMapping("/403")
    String error403() {
        return "/vue/index.html";
    }

}
