package com.zscat.platform.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zscat.common.utils.Query;
import com.zscat.common.utils.R;
import com.zscat.user.entity.CouponDO;
import com.zscat.user.service.CouponService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zscat
 * @email 951449465@qq.com
 * @date 2017-10-18 13:12:04
 */

@Controller
@RequestMapping("/shop/coupon")
public class CouponController {
    @Reference(
            version = "${web.service.version}",
            application = "${dubbo.application.id}",
            registry = "${dubbo.registry.id}"
    )
    private CouponService couponService;

    /**
     * 列表
     *
     * @param params
     * @return
     */
    @ResponseBody
    @GetMapping("/list")
    R list(@RequestParam Map<String, Object> params) {
        R r = new R();
        // 查询列表数据
        Query query = new Query(params);
        List<CouponDO> list = couponService.list(query);
        int total = couponService.count(query);
        r.put("rows", list);
        r.put("total", total);
        return r;
    }

    /**
     * 详情
     *
     * @param model
     * @param id
     * @return
     */
    @ResponseBody
    @GetMapping("/detail")
    R detail(Model model, Long id) {
        R r = new R();
        CouponDO sysDept = couponService.get(id);
        r.put("entity", sysDept);
        return r;
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(@RequestBody CouponDO entity) {

        if (entity.getId() == null) {
            if (couponService.save(entity) > 0) {
                return R.ok();
            }
        } else {
            if (couponService.update(entity) > 0) {
                return R.ok();
            }
        }
        return R.error();
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    // @RequiresPermissions("shop:coupon:edit")
    public R update(CouponDO coupon) {
        couponService.update(coupon);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    // @RequiresPermissions("shop:coupon:remove")
    public R remove(Long id) {
        if (couponService.remove(id) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    // @RequiresPermissions("shop:coupon:batchRemove")
    public R remove(@RequestParam("ids[]") Long[] ids) {
        couponService.batchRemove(ids);
        return R.ok();
    }

}
