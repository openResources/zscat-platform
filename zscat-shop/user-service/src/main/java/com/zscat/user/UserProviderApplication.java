package com.zscat.user;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * DubboProviderApplication
 * 服务提供启动类
 * * @author zscat
 * @date 2018/6/7
 */
@SpringBootApplication
@EnableScheduling
@MapperScan(basePackages = "com.zscat.user.dao")
@EnableTransactionManagement
@EnableAspectJAutoProxy
@ComponentScan("com.zscat")
@EnableDubbo
public class UserProviderApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(UserProviderApplication.class)
                .run(args);
    }
}
