package com.zscat.order;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * DubboProviderApplication
 * 服务提供启动类
 * * @author zscat
 * @date 2018/6/7
 */
@SpringBootApplication
@MapperScan(basePackages = "com.zscat.order.dao")
@EnableTransactionManagement
@EnableAspectJAutoProxy
@ComponentScan("com.zscat")
@EnableDubbo
public class OrderProviderApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(OrderProviderApplication.class)
                .web(WebApplicationType.NONE)
                .run(args);
    }
}
