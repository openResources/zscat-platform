//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.zscat.common.result;

import java.io.Serializable;

public class ResultInfo implements Serializable {
    private static final long serialVersionUID = 8694461719746019858L;
    private int code;
    private String msg = "";
    private int retcode;
    public ResultInfo() {
    }

    public ResultInfo(int retcode, String retdesc) {
        this.code = retcode;
        this.msg = retdesc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getRetcode() {
        return retcode;
    }

    public void setRetcode(int retcode) {
        this.retcode = retcode;
    }
}
